This is the repository for the 2018 Mathematical and Computational Methods in Geophysics course.

Try checking it out in Jupyter using:

`!git clone https://git.ecdf.ed.ac.uk/mnaylor/MathAndComp_StudentRepo_2018.git`

You can then pull the latest version by navigating into the MathAndComp_StudentRepo_2018 folder and using a new Jupyter notebook to run :

```!git stash
!git pull
!git stash pop```

The process of using a stash should ensure you do not overwrite the notebooks you have edited!

To be doubly safe - you might want to consider saving the folders you have been working on into a separate directory.
